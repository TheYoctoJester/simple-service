FROM theyoctojester/justnode:justnode-noicu-licenses

COPY dist/index.js /home/node/index.js

USER node

ENTRYPOINT [ "node", "/home/node/index.js" ]